var fs = require('fs');
var path = require('path');
var commands = [];

fs.readdirSync(path.join(__dirname, 'commands')).forEach(function(file) {
  commands.push(file.replace('.js', ''));
});

function register(client) {
  commands.forEach(function(name) {
    client.addCommand(name, require('./commands/' + name).bind(client));
  });
}

module.exports = {
  register: register
};

