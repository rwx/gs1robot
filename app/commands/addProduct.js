var config = require('../../config.json');
var handlers = {
  textInput: setTextFieldValue,
  checkbox: setCheckBoxValue,
  autoCompleteInput: setAutoCompleteValue,
  selectInput: setSelectValue
};
var client;

function addProduct(product, cb) {
  client = this;
  client.pause(500);

  config.form.fillOrder.forEach(function(field) {
    if (product[field]) {
      var type = config.form.type[field];
      handlers[type](field, product[field]);
    }
  });

  submitForm();

  client.call(cb);
};

function setSelectValue(field, value) {
  var selector = getSelectorForField(field);
  var valSelector = '//UL/LI[@class="x-boundlist-item" and text()="' + value + '"]';

  if (selector && value) {
    client.isVisible(selector, function(err, isVisible) {
      if (isVisible) {
        client.click(selector);
        client.waitForExist(valSelector);
        client.click(valSelector);
        client.waitForExist(valSelector, true);
      }
    });
  }
}

function setTextFieldValue(field, value) {
  var selector = getSelectorForField(field);
  if (selector && value) {
    client.isVisible(selector, function(err, isVisible) {
      if (isVisible) {
        client.addValue(selector, value);
      }
    });
  }
}

function setCheckBoxValue(field, value) {
  var expectedValue = !!value;
  var selector = getSelectorForField(field);
  if (selector) {
    client.isSelected(selector, function(err, currentValue) {
      if (currentValue !== expectedValue) {
        client.click(selector);
      }
    });
  }
}

function setAutoCompleteValue(field, value) {
  var selector = getSelectorForField(field);
  var valSelector = '//ul/li[contains(@class, "x-boundlist-item")]/b[text()="' + value + ':"]/ancestor::li';
  var maskSelector = valSelector + '/ancestor::div[contains(@class, "x-boundlist")]/following-sibling::div[contains(@class, "x-mask-msg")]';

  if (selector && value) {
    client.clearElement(selector);
    client.addValue(selector, value);
    client.waitForExist(valSelector, function() {
      client.waitForVisible(maskSelector, 1500, true, function() {
        client.click(valSelector);
        client.waitForExist(valSelector, true);
      })
    });
  }
}

function submitForm() {
  client.click(config.form.selectors.SUBMIT_BUTTON);
  client.waitForExist(config.form.selectors.KODAS, 2000, true);
}

function getSelectorForField(field) {
  return config.form.selectors[field];
}

module.exports = addProduct;
