var products = require('../../data/products');

function addProducts(cb) {
  var client = this;

  products.forEach(function(product) {
    client.openProductForm();
    client.addProduct(product);
  });
}

module.exports = addProducts;
