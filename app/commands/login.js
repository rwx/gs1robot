var config = require('../../config.json');

function login(cb) {
  this.click('//body/div[1]//*[text()="Prisijungti"]/..')
  this.waitFor('input[name="WU_LOGIN"]')
  this.addValue('input[name="WU_LOGIN"]', config.connection.user)
  this.addValue('input[name="WU_PASSWORD"]', config.connection.pass)
  this.click('//*[contains(@id, "loginwindow")]//*[text()="Prisijungti"]/..')
  this.waitFor('//*[contains(@class, "x-tab-bar")]//*[text()="Paraiškos"]')
  this.call(cb);
}

module.exports = login;

