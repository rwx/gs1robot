
function navigateToProductList(cb) {
  this.click('//*[contains(@class, "x-tab-bar")]//*[text()="Paraiškos"]/..');
  this.waitForExist('#paraiskosPrekeGrid', 2000);
  this.call(cb);
}

module.exports = navigateToProductList;

