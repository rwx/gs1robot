
function openProductForm(cb) {
  this.click('//*[contains(@id, "paraiskosPrekeGrid")]//*[text()="Sukurti"]/..');
  this.waitForExist('input[name="PARP_PREKE"]', 2000);
  this.call(cb);
}

module.exports = openProductForm;

