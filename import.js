var config = require('./config.json');
var commands = require('./app/commands');
var client = require('webdriverio').remote(config.driver).init();

commands.register(client);

client.url(config.connection.host).run();

